package de.htwaalen.project.propas.common;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "AssignmentList")
public class AssignmentListPOJO {
	private List<AssignmentPOJO> assignmentList;

	public AssignmentListPOJO(){}
	
	public AssignmentListPOJO(List<AssignmentPOJO> assignmentList){
		setAssignmentList(assignmentList);
	}
	
	@XmlElement(name = "Assignment")
	public List<AssignmentPOJO> getAssignmentList() {
		return assignmentList;
	}

	public void setAssignmentList(List<AssignmentPOJO> assignmentList) {
		this.assignmentList = assignmentList;
	}
	
	
}
