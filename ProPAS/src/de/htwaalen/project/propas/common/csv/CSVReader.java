/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package de.htwaalen.project.propas.common.csv;

import com.Ostermiller.util.CSVParser;
import de.htwaalen.project.propas.common.AssignmentPOJO;
import de.htwaalen.project.propas.common.ResourcePOJO;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;

/**
 *
 * @author norabx
 */
public class CSVReader {
	protected final int ASSIGNMENT = 0;
	protected final int RESOURCE = 1;
	
    protected String[][] csvAssignmentsValue;
    protected String[][] csvResourcesValue;
    
    protected HashMap<String,Integer> csvAssignmentHeadings = new HashMap<String,Integer>();
    protected HashMap<String,Integer> csvResourceHeadings = new HashMap<String,Integer>();
    
    protected final String[] TIMEPATTERN = new String[]{"yyyy-MM-dd'T'HH:mm:ss"};
        
    public List<AssignmentPOJO> readAssignments(String filename){
    	csvAssignmentsValue = readCSVFile(filename);
        if(csvAssignmentsValue != null){
        	
            for(int i = 0; i < csvAssignmentsValue[0].length; i++){
            	csvAssignmentHeadings.put(csvAssignmentsValue[0][i], i);
            }
            
            List<AssignmentPOJO> assignmentList = new ArrayList<AssignmentPOJO>();
            for(int i = 1; i < csvAssignmentsValue.length; i++){
            	
                AssignmentPOJO assignment = new AssignmentPOJO();
                assignment.setID(parseIntArgument("ID",i,ASSIGNMENT));
                assignment.setName(getCSVValue("Name",i,ASSIGNMENT));
                assignment.setPriority(parseIntArgument("Priority",i,ASSIGNMENT));
                assignment.setEstimatedDuration(parseDoubleArgument("EstimatedDuration",i,ASSIGNMENT));
                assignment.setSize(parseDoubleArgument("Size",i,ASSIGNMENT));             
                
                assignmentList.add(assignment);
            }
            
            return assignmentList;
        }else{ return null; }
    }
    
    public List<ResourcePOJO> readResources(String filename){
    	csvResourcesValue = readCSVFile(filename);
        if(csvResourcesValue != null){
        	
            for(int i = 0; i < csvResourcesValue[0].length; i++){
            	csvResourceHeadings.put(csvResourcesValue[0][i], i);
            }
            
            List<ResourcePOJO> resourceList = new ArrayList<ResourcePOJO>();
            for(int i = 1; i < csvResourcesValue.length; i++){
            	
                ResourcePOJO resource = new ResourcePOJO();
                resource.setID(parseIntArgument("ID",i,RESOURCE));
                resource.setName(getCSVValue("Name",i,RESOURCE));
                resource.setRoles(Arrays.asList(parseStringArrayArgument("Roles",i,RESOURCE)));
                resource.setExperienceLevel(getCSVValue("ExperienceLevel",i,RESOURCE));
                resource.setUtilizationMaximum(parseDoubleArgument("UtilizationMaximum",i,RESOURCE));
                resource.setWipLimit(parseIntArgument("WipLimit",i,RESOURCE));
                
                resourceList.add(resource);
            }
            
            return resourceList;
        }else{ return null; }
    }
    
    protected String[][] readCSVFile(String filename){
    	InputStreamReader in = null;
        try{
        	in = new InputStreamReader(new FileInputStream(filename));
            CSVParser parser = new CSVParser(in,',');
            return parser.getAllValues();
        } catch (IOException ex) {
            Logger.getLogger(CSVReader.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    protected int parseIntArgument(String arg, int index, int flag){
        return parseInt(getCSVValue(arg,index,flag));
    }
    
    protected int parseInt(String value){
        if(StringUtils.isNumeric(value)){ return Integer.parseInt(value); }
        else{ throw new IllegalArgumentException(value + " is not a Number"); }    
    }
    
    protected double parseDoubleArgument(String arg, int index, int flag){
        return parseDouble(getCSVValue(arg,index,flag));
    }
    
    protected double parseDouble(String value){
        if(StringUtils.isNumeric(value)){ return Double.parseDouble(value); }
        else{ throw new IllegalArgumentException(value + " is not a Number"); }
    }
    
    protected String[] parseStringArrayArgument(String arg, int index, int flag){
        return StringUtils.split(getCSVValue(arg,index,flag),",");
    }
    
    protected int[] parseIntArrayArgument(String arg, int index, int flag){
        String[] value = parseStringArrayArgument(arg,index,flag);
        int[] idvalue = null;
        
        if(ArrayUtils.isNotEmpty(value)){
            idvalue = new int[value.length];
            
            for(int i = 0; i < idvalue.length; i++){
                idvalue[i] = parseInt(value[i]);
            }
        }
        
        return idvalue;
    }
    
    
    protected boolean parseBooleanArgument(String arg, int index, int flag){
        Boolean value = BooleanUtils.toBooleanObject(getCSVValue(arg,index,flag));
        
        if(value != null){ return BooleanUtils.toBoolean(value); }
        else{ throw new IllegalArgumentException(arg + " is not Boolean"); }
    }
    
    /*
    protected Date parseDateArgument(String arg, int index){
        Date date = null;
        String value = getCSVValue(arg,index);
        
        if(StringUtils.isNotEmpty(value)){
            try{ date = DateUtils.parseDate(value, TIMEPATTERN); } 
            catch(ParseException ex){}
        }
        
        return date;
    }
    */
    
    protected String getCSVValue(String arg, int index, int flag){
    	
    	switch(flag){
	    	case ASSIGNMENT: if(csvAssignmentHeadings.containsKey(arg)){ return csvAssignmentsValue[index][csvAssignmentHeadings.get(arg)]; };
	    	case RESOURCE: if(csvResourceHeadings.containsKey(arg)){ return csvResourcesValue[index][csvResourceHeadings.get(arg)]; };
	    	default: throw new IllegalArgumentException(arg + " Column Heading is not available");
    	}
    }
}