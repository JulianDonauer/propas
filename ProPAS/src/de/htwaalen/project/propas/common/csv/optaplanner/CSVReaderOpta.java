/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package de.htwaalen.project.propas.common.csv.optaplanner;

import de.htwaalen.project.propas.common.AssignmentPOJO;
import de.htwaalen.project.propas.common.ResourcePOJO;
import de.htwaalen.project.propas.common.csv.CSVReader;
import de.htwaalen.project.propas.common.optaplanner.AssignmentOpta;
import de.htwaalen.project.propas.common.optaplanner.ResourceOpta;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author norabx
 */
public class CSVReaderOpta extends CSVReader{

    public List<AssignmentOpta> readAssignmentsOpta(String filename){
        List<AssignmentPOJO> assignmentList = readAssignments(filename);
        List<AssignmentOpta> newAssignmentList = new ArrayList<AssignmentOpta>();
        
        if(assignmentList != null){
            for(int i = 1; i < assignmentList.size() + 1; i++){
                AssignmentOpta assignment = new AssignmentOpta(assignmentList.get(i - 1));
                
                //assignment.setPredeccessorIDs(parseIntArrayArgument("PredeccessorIDs",i,ASSIGNMENT));
                assignment.setSuccessorIDs(parseIntArrayArgument("SuccessorIDs",i,ASSIGNMENT));
                assignment.setRequiredCompetencies(parseStringArrayArgument("RequiredCompetencies",i,ASSIGNMENT));
                assignment.setSuggestedCompetencies(parseStringArrayArgument("SuggestedCompetencies",i,ASSIGNMENT));
                assignment.setRequiredRoles(parseStringArrayArgument("RequiredRoles",i,ASSIGNMENT));
                assignment.setSuggestedRoles(parseStringArrayArgument("SuggestedRoles",i,ASSIGNMENT));
                assignment.setPgp(parseBooleanArgument("PGP",i,ASSIGNMENT));
                
                newAssignmentList.add(assignment);
            }
        }
        
        return newAssignmentList;
    }
    
    public List<ResourceOpta> readResourcesOpta(String filename){
        List<ResourcePOJO> resourceList = readResources(filename);
        List<ResourceOpta> newResourceList = new ArrayList<ResourceOpta>();
        
        if(resourceList != null){
            for(int i = 1; i < resourceList.size() + 1; i++){
                ResourceOpta resource = new ResourceOpta(resourceList.get(i - 1));
                resource.setCompetencies(parseStringArrayArgument("Competencies",i,RESOURCE));
            
                newResourceList.add(resource);
            }
        }
        
        return newResourceList;
    }
}
