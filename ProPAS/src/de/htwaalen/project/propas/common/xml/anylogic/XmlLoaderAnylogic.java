package de.htwaalen.project.propas.common.xml.anylogic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import de.htwaalen.project.propas.common.AssignmentPOJO;
import de.htwaalen.project.propas.common.anylogic.Profile;
import de.htwaalen.project.propas.common.anylogic.activities.Activities;

/**
 * @author Joule
 *
 */
public class XmlLoaderAnylogic {

	private ConfigurationLocation configLocation;
	private JAXBContext context;
	private Marshaller marshaller;
	private RoleActivityMapper mapper;
	
	public Activities getActivities(){
		Activities activities = null;
		try{
		    	JAXBContext context = JAXBContext.newInstance( Activities.class );
		    	File file = new File(configLocation.getActivitiesConfigLocation());
				activities = (Activities) context.createUnmarshaller().unmarshal(file);
				activities.init();
	    }    	
		catch(JAXBException e){
				e.printStackTrace();
	    }		
		return activities;
	}
	
	public Profile getProfil(List<String> roleNames){
		try{
			Profile profile = null;
			if(roleNames == null){
				context = JAXBContext.newInstance( Profile.class );
				File file = new File(configLocation.getResourceAny("Default"));
				profile = (Profile) context.createUnmarshaller().unmarshal(file);
				return profile;
			}
			for(int i=0;i<roleNames.size();i++){
				if(i==0){
					context = JAXBContext.newInstance( Profile.class );
			    	File file = new File(configLocation.getResourceAny(roleNames.get(i)));
					profile = (Profile) context.createUnmarshaller().unmarshal(file);
				}
				else{
					if(profile != null) {
						String act = mapper.getActivity(roleNames.get(i));
						if(act != null) profile.changeActivityEPH(configLocation.getActivityEPH(act));
					}
				}
			}
			return profile;
	    	
    	}
		catch(JAXBException e){
			e.printStackTrace();
    	}
		return null;
	}
	
	/**
	 * Init
	 * @param configLocation
	 */
	public void init(String configLocation){
		try{
	    	context = JAXBContext.newInstance( ConfigurationLocation.class );
	    	File file = new File(configLocation);
			this.configLocation = (ConfigurationLocation) context.createUnmarshaller().unmarshal(file);
			
			context = JAXBContext.newInstance( RoleActivityMapper.class );
	    	file = new File(this.configLocation.getRoleActivityMapper());
			this.mapper = (RoleActivityMapper) context.createUnmarshaller().unmarshal(file);
    	}
		catch(JAXBException e){
			e.printStackTrace();
    	}
	}
	
	/**
	 * Constructor to create an object of this class
	 * @param configLocation <br>
	 *  			Location of the configuration file <br>
	 *  			{@link ConfigurationLocation}
	 * 
	 */
	public XmlLoaderAnylogic(String configLocation){
		init(configLocation);
	}
	

	//TEST---------------------------------------------------------------------------
	public ConfigurationLocation getConfigurationLocation(){
		return configLocation;
	}
	
	
}
