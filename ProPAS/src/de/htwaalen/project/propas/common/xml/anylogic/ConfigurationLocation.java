package de.htwaalen.project.propas.common.xml.anylogic;
import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import de.htwaalen.project.propas.common.anylogic.ActivityEPH;
import de.htwaalen.project.propas.common.xml.anylogic.ProfileLocation;;

@XmlRootElement(name = "ConfigurationLoader")
public class ConfigurationLocation {
	

	private String projectConfigLocation;
	private String activitiesConfigLocation;
	private String defaultProfile;
	private String roleActivityMapper;
	private ArrayList<ProfileLocation> profileList;
	private ArrayList<ActivityEPH> activitiesEPH;
	
	@XmlElement(name = "RoleActivityMapper")
	public String getRoleActivityMapper() {
		return roleActivityMapper;
	}
	public void setRoleActivityMapper(String roleActivityMapper) {
		this.roleActivityMapper = roleActivityMapper;
	}
	
	
	@XmlElement(name = "ProjectConfiguration")
	public String getProjectConfigLocation() {
		return projectConfigLocation;
	}
	public void setProjectConfigLocation(String projectConfigLocation) {
		this.projectConfigLocation = projectConfigLocation;
	}
	
	@XmlElement(name = "ActivitiesConfiguration")
	public String getActivitiesConfigLocation() {
		return activitiesConfigLocation;
	}
	public void setActivitiesConfigLocation(String activitiesConfigLocation) {
		this.activitiesConfigLocation = activitiesConfigLocation;
	}
	
	@XmlElement(name = "ProfileList")
	public ArrayList<ProfileLocation> getProfileList() {
		return profileList;
	}
	
	public void setProfileList(ArrayList<ProfileLocation> profilList) {
		this.profileList = profilList;
	}

	@XmlElement(name = "DefaultProfile")
	public String getDefaultProfile() {
		return defaultProfile;
	}
	public void setDefaultProfile(String defaultProfile) {
		this.defaultProfile = defaultProfile;
	}
	
	
	@XmlElement(name = "ActivitiesEPH")
	public ArrayList<ActivityEPH> getActivitiesEPH() {
		return activitiesEPH;
	}
	public void setActivitiesEPH(ArrayList<ActivityEPH> activitiesEPH) {
		this.activitiesEPH = activitiesEPH;
	}
	
	
	public String getResourceAny(String role){
		for(int i=0;i<profileList.size();i++){
			if(profileList.get(i).getRoleName().equals(role)){
				return profileList.get(i).getProfilLocation();
			}
		}
		return defaultProfile;
	}
	
	
	public ActivityEPH getActivityEPH(String role){
		System.out.println("Suche nach rollen");
		for(int i=0;i<activitiesEPH.size();i++){
			System.out.println(role + activitiesEPH.get(i).getName());
			if(activitiesEPH.get(i).getName().equals(role))
				return activitiesEPH.get(i);
		}
		return null;
	}
	
	
	
	
	
	/**
	 * Default Constructor
	 */
	public ConfigurationLocation(){
		
	}
	
	public ConfigurationLocation(String projectConfiguration, String activitiesConfiguration,
						ArrayList<ProfileLocation> list, String defaultProfile){
		this.projectConfigLocation = projectConfiguration;
		this.activitiesConfigLocation = activitiesConfiguration;
		this.profileList = list;
		this.defaultProfile = defaultProfile;
	}
	
}
