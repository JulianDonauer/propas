package de.htwaalen.project.propas.common.xml.anylogic;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "RoleActivityMapper")
public class RoleActivityMapper {


	private ArrayList<RoleActivityPair> pairs;
	
	
	@XmlElement(name = "Pairs")
	public ArrayList<RoleActivityPair> getPairs() {
		return pairs;
	}

	public void setPairs(ArrayList<RoleActivityPair> pairs) {
		this.pairs = pairs;
	}
	
	public String getActivity(String role){
		for(int i=0;i<pairs.size();i++){
			if(pairs.get(i).getRole().equals(role)){
				return pairs.get(i).getActivity();
			}
		}
		return null;
	}

}
