package de.htwaalen.project.propas.common.xml.common;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import de.htwaalen.project.propas.common.AssignmentListPOJO;
import de.htwaalen.project.propas.common.AssignmentPOJO;
import de.htwaalen.project.propas.common.PlanPOJO;
import de.htwaalen.project.propas.common.ResourceListPOJO;
import de.htwaalen.project.propas.common.ResourcePOJO;

public class XmlParser {
	
	private static JAXBContext context;
	private static Marshaller marshaller;

	
	
	// AssignmentPOJO -------------------------------------------------------------------------------------------------
	public static String assignmentToXML(AssignmentPOJO assi){
		try {
			context = JAXBContext.newInstance( AssignmentPOJO.class );
			marshaller = context.createMarshaller();
			StringWriter sw = new StringWriter();
            marshaller.marshal(assi, sw);
            return sw.toString();
			
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public static AssignmentPOJO xmlToAssignment(String assi){
		try {
			context = JAXBContext.newInstance(AssignmentPOJO.class);
			Unmarshaller unmarshaller = context.createUnmarshaller();

			StringReader reader = new StringReader(assi);
			return (AssignmentPOJO) unmarshaller.unmarshal(reader);
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
		
	}
	
	
	// ResourcePOJO -------------------------------------------------------------------------------------------------
	public static String resourceToXML(ResourcePOJO res){
		try {
			context = JAXBContext.newInstance( ResourcePOJO.class );
			marshaller = context.createMarshaller();
			StringWriter sw = new StringWriter();
            marshaller.marshal(res, sw);
            return sw.toString();
			
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public static ResourcePOJO xmlToResource(String res){
		try {
			context = JAXBContext.newInstance(ResourcePOJO.class);
			Unmarshaller unmarshaller = context.createUnmarshaller();

			StringReader reader = new StringReader(res);
			return (ResourcePOJO) unmarshaller.unmarshal(reader);
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
		
	}

	
	
	// AssignmentListPOJO ---------------------------------------------------------------------------------------
	public static String assignmentListToXML(AssignmentListPOJO assignmentList){
		try {
			context = JAXBContext.newInstance( AssignmentListPOJO.class );
			marshaller = context.createMarshaller();
			StringWriter sw = new StringWriter();
            marshaller.marshal(assignmentList, sw);
            return sw.toString();
			
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public static AssignmentListPOJO xmlToAssignmentList(String assignmentList){
		try {
			context = JAXBContext.newInstance(AssignmentListPOJO.class);
			Unmarshaller unmarshaller = context.createUnmarshaller();

			StringReader reader = new StringReader(assignmentList);
			return (AssignmentListPOJO) unmarshaller.unmarshal(reader);
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
		
	}
	
	// ResourceListPOJO -------------------------------------------------------------------------------------------------
	public static String resourceListToXML(ResourceListPOJO resourceList){
		try {
			context = JAXBContext.newInstance( ResourceListPOJO.class );
			marshaller = context.createMarshaller();
			StringWriter sw = new StringWriter();
            marshaller.marshal(resourceList, sw);
            return sw.toString();
			
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public static ResourceListPOJO xmlToResourceList(String resourceList){
		try {
			context = JAXBContext.newInstance(ResourceListPOJO.class);
			Unmarshaller unmarshaller = context.createUnmarshaller();

			StringReader reader = new StringReader(resourceList);
			return (ResourceListPOJO) unmarshaller.unmarshal(reader);
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	
	//PlanPOJO ------------------------------------------------------------------------------------------------------------
	public static String planPOJOToXML(PlanPOJO planPOJO){
		try {
			context = JAXBContext.newInstance( PlanPOJO.class );
			marshaller = context.createMarshaller();
			StringWriter sw = new StringWriter();
            marshaller.marshal(planPOJO, sw);
            return sw.toString();
			
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public static PlanPOJO xmlToPlanPOJO(String planPOJO){
		try {
			context = JAXBContext.newInstance(PlanPOJO.class);
			Unmarshaller unmarshaller = context.createUnmarshaller();

			StringReader reader = new StringReader(planPOJO);
			return (PlanPOJO) unmarshaller.unmarshal(reader);
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
		
	}

}
