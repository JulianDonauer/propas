package de.htwaalen.project.propas.common.xml.anylogic;
import javax.xml.bind.annotation.XmlElement;


public class ProfileLocation {
	private String roleName;
	private String profilLocation;
	
	@XmlElement(name = "RoleName")
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	
	@XmlElement(name = "Location")
	public String getProfilLocation() {
		return profilLocation;
	}
	public void setProfilLocation(String profilLocation) {
		this.profilLocation = profilLocation;
	}
	
	public ProfileLocation(){
		
	}
	
	public ProfileLocation(String roleName, String profilLocation){
		this.roleName = roleName;
		this.profilLocation = profilLocation;
	}
	
	
}
