package de.htwaalen.project.propas.common.xml.anylogic;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "ActivityRolePair")
public class RoleActivityPair {
	
	@XmlElement(name = "Activity")
	public String getActivity() {
		return activity;
	}
	
	@XmlElement(name = "Role")
	public String getRole() {
		return role;
	}
	public void setActivity(String activity) {
		this.activity = activity;
	}
	public void setRole(String role) {
		this.role = role;
	}
	private String activity;
	private String role;
	
	public RoleActivityPair(){
		
	}
	
	public RoleActivityPair(String role, String activity){
		this.role = role;
		this.activity = activity;
	}
	

}
