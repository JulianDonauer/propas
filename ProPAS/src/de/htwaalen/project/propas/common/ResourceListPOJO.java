package de.htwaalen.project.propas.common;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "ResourceList")
public class ResourceListPOJO {
	private List<ResourcePOJO> resourceList;

	public ResourceListPOJO(){}
	
	public ResourceListPOJO(List<ResourcePOJO> resourceList){
		setResourceList(resourceList);
	}
	
	@XmlElement(name = "Resource")
	public List<ResourcePOJO> getResourceList() {
		return resourceList;
	}

	public void setResourceList(List<ResourcePOJO> resourceList) {
		this.resourceList = resourceList;
	}
	
	
}
