package de.htwaalen.project.propas.common;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "Plan")
public class PlanPOJO {
	private List<AllocationPOJO> list;

	
	@XmlElement(name = "Assignments")
	public List<AllocationPOJO> getList() {
		return list;
	}

	public void setList(List<AllocationPOJO> list) {
		this.list = list;
	}
	
	
}
