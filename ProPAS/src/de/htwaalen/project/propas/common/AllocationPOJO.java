package de.htwaalen.project.propas.common;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;

@XmlRootElement(name = "Allocation")
public class AllocationPOJO {
	
	private int AssignmentID;
	private Date startTime;
	private List<Integer> resourceIDs;
	
	
	@XmlElement(name = "StartTime")
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	@XmlElement (name = "AssignmentID")
	public int getAssignmentID() {
		return AssignmentID;
	}
	public void setAssignmentID(int assignmentID) {
		AssignmentID = assignmentID;
	}
	
	@XmlElement (name = "ResourceIDs")
	public List<Integer> getResourceIDs() {
		return resourceIDs;
	}
	public void setResourceIDs(List<Integer> resourceIDs) {
		this.resourceIDs = resourceIDs;
	}
	
	

}
