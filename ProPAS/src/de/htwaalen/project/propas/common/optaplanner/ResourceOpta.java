/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package de.htwaalen.project.propas.common.optaplanner;

import de.htwaalen.project.propas.common.ResourcePOJO;
import java.util.List;

/**
 *
 * @author norabx
 */
public class ResourceOpta extends ResourcePOJO{
    private String[] competencies;

    public ResourceOpta(ResourcePOJO resource){
        setID(resource.getID());
        setName(resource.getName());
        setRoles(resource.getRoles());
        setExperienceLevel(resource.getExperienceLevel());
        setUtilizationMaximum(resource.getUtilizationMaximum());
        setWipLimit(resource.getWipLimit());
    }
    
    public String[] getCompetencies() {
        return competencies;
    }

    public void setCompetencies(String[] competencies) {
        this.competencies = competencies;
    }
    
    
}
