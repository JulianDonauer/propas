/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package de.htwaalen.project.propas.common.optaplanner;

import de.htwaalen.project.propas.common.AssignmentPOJO;

/**
 *
 * @author norabx
 */
public class AssignmentOpta extends AssignmentPOJO{
    
	
    
    private int[] successorIDs;
    private String[] requiredCompetencies;
    private String[] suggestedCompetencies;
    private String[] requiredRoles;
    private String[] suggestedRoles;
    private boolean pgp; //priority trumps profile
    
    public AssignmentOpta(AssignmentPOJO assignment){
        setID(assignment.getID());
        setName(assignment.getName());
        setPriority(assignment.getPriority());
        setEstimatedDuration(assignment.getEstimatedDuration());
        setSize(assignment.getSize());
    }

    

    public int[] getSuccessorIDs() {
        return successorIDs;
    }

    public String[] getRequiredCompetencies() {
        return requiredCompetencies;
    }

    public String[] getSuggestedCompetencies() {
        return suggestedCompetencies;
    }

    public String[] getRequiredRoles() {
        return requiredRoles;
    }

    public String[] getSuggestedRoles() {
        return suggestedRoles;
    }

    public boolean isPgp() {
        return pgp;
    }

    

    public void setSuccessorIDs(int[] successorIDs) {
        this.successorIDs = successorIDs;
    }

    public void setRequiredCompetencies(String[] requiredCompetencies) {
        this.requiredCompetencies = requiredCompetencies;
    }

    public void setSuggestedCompetencies(String[] suggestedCompetencies) {
        this.suggestedCompetencies = suggestedCompetencies;
    }

    public void setRequiredRoles(String[] requiredRoles) {
        this.requiredRoles = requiredRoles;
    }

    public void setSuggestedRoles(String[] suggestedRoles) {
        this.suggestedRoles = suggestedRoles;
    }

    public void setPgp(boolean pgp) {
        this.pgp = pgp;
    }
}
