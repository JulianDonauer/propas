package de.htwaalen.project.propas.common.anylogic.activities;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "Destination")
public class Destination {
	private String name;
	private double value;
	
	@XmlElement(name = "Name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@XmlElement(name = "Value")
	public double getValue() {
		return value;
	}
	public void setValue(double value) {
		this.value = value;
	}
	
	public Destination(){
		
	}
	
	public Destination(String name, double value){
		this.name = name;
		this.value = value;
	}
}
