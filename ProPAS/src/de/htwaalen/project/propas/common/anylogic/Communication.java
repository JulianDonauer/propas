package de.htwaalen.project.propas.common.anylogic;

import java.net.ConnectException;

import de.htwaalen.project.propas.common.AssignmentPOJO;
import de.htwaalen.project.propas.common.ResourcePOJO;
import de.htwaalen.project.propas.common.netty.anylogic.Client;
import de.htwaalen.project.propas.common.netty.common.Version;

public class Communication {

	private boolean response;
	private Client client;
	
	public Communication(){
		
	}
	
	public AssignmentPOJO getNewAssignment(){
		AssignmentPOJO assi = null;
		client.sendMessage(Version.INITASSIGNMENT);
		while(assi == null){
			try {
				assi = (AssignmentPOJO)client.getResponse();
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		};
		return assi;
	}
	
	public ResourcePOJO getResource(){
		ResourcePOJO res = null;
		client.sendMessage(Version.INITRESOURCE);
		while(res == null){
			try {
				res = (ResourcePOJO)client.getResponse();
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		};
		return res;
	}
	
	public boolean isAlive(){
		Boolean res = null;
		client.sendMessage(Version.ISALIVE);
		long start = System.currentTimeMillis();
		while(res == null || System.currentTimeMillis() < start + 1000){
			try {
				res = (Boolean)client.getResponse();
				if(res != null && res == true) return true;
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return false;
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean connect(){
		try {
			client = new Client("localhost", 9999);
			return client.start();
		} 
		catch (Exception e) {
			return false;
		}
	}
	
	
	
	public void setResponse(boolean b){
		this.response = b;
	}
	
	
	
}
