/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package de.htwaalen.project.propas.common.anylogic;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import de.htwaalen.project.propas.common.ResourcePOJO;

/**
 *
 * @author norabx
 */
@XmlRootElement(name = "ResourceAny")
public class Profile{
	




	private String Role;
	
    private double productivityLose;
    private double mulitplierDueLearning = 1.0;
    
	
    
    //Working-Time
    private double startTime;
    
    private ArrayList<ActivityEPH> activitiesEPH;

    
    @XmlElement(name = "Role")
	public String getRole() {
		return Role;
	}

	public void setRole(String role) {
		Role = role;
	}
    
    @XmlElement(name = "ProductivityLose")
	public double getProductivityLose() {
		return productivityLose;
	}


	
	@XmlElement(name = "StartTime")
	public double getStartTime() {
		return startTime;
	}

	@XmlElement(name = "ActivitiesEPH")
	public ArrayList<ActivityEPH> getActivitiesEPH() {
		return activitiesEPH;
	}

	
	
	
	public void setProductivityLose(double productivityLose) {
		this.productivityLose = productivityLose;
	}



	public void setStartTime(double startTime) {
		this.startTime = startTime;
	}

	

	public void setActivitiesEPH(ArrayList<ActivityEPH> activitiesEPH) {
		this.activitiesEPH = activitiesEPH;
	}
    
	
	public void changeActivityEPH(ActivityEPH actEPH){
		for(int i=0;i<activitiesEPH.size();i++){
			if(activitiesEPH.get(i).getName().equals(actEPH.getName())){
				activitiesEPH.remove(i);
			}
		}
		activitiesEPH.add(actEPH);
		
	}
	
	
	
	
	public void setMultiplierDueLearning(double multi){
    	this.mulitplierDueLearning = multi;
    }
	
	public double getEPH(String activity){
		for(int i=0;i<activitiesEPH.size();i++){
			if(activitiesEPH.get(i).getName().equals(activity)){
				return activitiesEPH.get(i).getEPZ() * (1-productivityLose) * mulitplierDueLearning;
			}
		}
		
		return 10.0 * (1-productivityLose) * mulitplierDueLearning;
	}
    
    
}
