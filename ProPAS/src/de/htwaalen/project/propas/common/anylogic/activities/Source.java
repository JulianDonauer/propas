package de.htwaalen.project.propas.common.anylogic.activities;

import java.util.ArrayList;
import java.util.TreeMap;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Source")
public class Source {
	
	private String name;
	private double affort;
	private ArrayList<Destination> list;
	private TreeMap<Double, String> mapList;
	
	@XmlElement(name = "Name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@XmlElement(name = "Destination")
	public ArrayList<Destination> getList() {
		return list;
	}
	public void setList(ArrayList<Destination> list) {
		this.list = list;
	}
	
	@XmlElement(name = "Effort")
	public double getAffort() {
		return affort;
	}
	public void setAffort(double affort) {
		this.affort = affort;
	}
	
	public void init(){
		double temp = 0;
		for(int i=0;i<list.size();i++){
			temp += list.get(i).getValue();
			mapList.put(temp, list.get(i).getName());
		}
	}
	
	public String getNextActivity(){
		double random = Math.random() * 100;
		return mapList.ceilingEntry(random).getValue();
	}
	
	public Source(){

		mapList = new TreeMap<Double, String>();
	}
	
	public Source(String name, double affort, ArrayList<Destination> list){
		this.name = name;
		this.list = list;
		this.affort = affort;
		mapList = new TreeMap<Double, String>();
	}
	
	

}
