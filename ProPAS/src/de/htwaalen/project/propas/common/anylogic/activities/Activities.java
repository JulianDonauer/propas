package de.htwaalen.project.propas.common.anylogic.activities;



import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Activities")
public class Activities {
	
	ArrayList<Source> list;
	
	private String firstActivity;

	@XmlElement(name = "FirstActivity")
	public String getFirstActivity(){
		return firstActivity;
	}
	
	public void setFirstActivity(String act){
		this.firstActivity = act;
	}
	
	@XmlElement(name = "Sources")
	public ArrayList<Source> getList() {
		return list;
	}

	public void setList(ArrayList<Source> list) {
		this.list = list;
	}
	
	public String[] getAllActivities(){
		String[] activities = new String[list.size()];
		for(int i=0;i<list.size();i++)
			activities[i] = list.get(i).getName();
		return activities;
	}
	
	public String getNextActivity(String activity){
		if(activity == null){
			return firstActivity;				
		}
		else{
			for(int i=0;i<list.size();i++)
				if(list.get(i).getName().equalsIgnoreCase(activity))
					return list.get(i).getNextActivity();
		}
		System.out.println("Nichts gefunden für " + activity);
		return null;
	}
	
	public double getAffort(String activity, int amount){
		int temp = (int)Math.pow(2, amount);
		for(int i=0;i<list.size();i++)
			if(list.get(i).getName().equalsIgnoreCase(activity))
				return list.get(i).getAffort() / temp;
		return 0;
	}
	
	public void init(){
		for(int i=0;i<list.size();i++){
			list.get(i).init();
		}
	}
	
	
	public Activities(ArrayList<Source> list){
		this.list = list;
	}
	
	public Activities(){
	}
	

}
