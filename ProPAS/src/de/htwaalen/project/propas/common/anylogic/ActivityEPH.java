package de.htwaalen.project.propas.common.anylogic;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

//acitivity effort per hour
@XmlRootElement(name ="ActivityEPH")
public class ActivityEPH {
	
	private String name;
	private double EPZ;
	
	
	@XmlElement(name = "Name")
	public String getName() {
		return name;
	}
	
	@XmlElement(name = "EPZ")
	public double getEPZ() {
		return EPZ;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setEPZ(double ePZ) {
		EPZ = ePZ;
	}
	
	public ActivityEPH(){
		
	}
	
	public ActivityEPH(String name, double epz){
		this.name = name;
		this.EPZ = epz;
	}

}
