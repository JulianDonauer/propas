package de.htwaalen.project.propas.common.anylogic;

import de.htwaalen.project.propas.common.ResourcePOJO;

public class ResourceAny extends ResourcePOJO{

	protected Profile profile;
	
	
	public Profile getProfile() {
		return profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}
	
	
	
	
}
