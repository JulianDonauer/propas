/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package de.htwaalen.project.propas.common;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author norabx
 */

@XmlRootElement(name = "Destination")
public class AssignmentPOJO {
    protected int ID;
    protected String name;
    protected int priority; // 0 - 10 ; 10 = critical
    protected double estimatedDuration; // min
    protected double size;

    protected Date startTime;
    protected Date endTime;
    
    protected List<ResourcePOJO> allocatedResourceList;
    protected List<Integer> predeccessorIDs;
    
    
    @XmlElement(name = "PredeccessorIDs")
    public List<Integer> getPredeccessorIDs() {
		return predeccessorIDs;
	}

	public void setPredeccessorIDs(List<Integer> predeccessorIDs) {
		this.predeccessorIDs = predeccessorIDs;
	}

	@XmlElement(name = "ID")
    public int getID() {
        return ID;
    }

    @XmlElement(name = "Priority")
    public int getPriority() {
        return priority;
    }

    @XmlElement(name = "EstimationDuration")
    public double getEstimatedDuration() {
        return estimatedDuration;
    }

    @XmlElement(name = "Size")
    public double getSize() {
        return size;
    }

    @XmlElement(name = "StartTime")
    public Date getStartTime() {
        return startTime;
    }

    @XmlElement(name = "EndTime")
    public Date getEndTime() {
        return endTime;
    }
    
    
    @XmlElement(name = "AllocatedResource")
    public List<ResourcePOJO> getAllocatedResourceList() {
		return allocatedResourceList;
	}

	public void setAllocatedResourceList(List<ResourcePOJO> allocatedResourceList) {
		this.allocatedResourceList = allocatedResourceList;
	}

	public void setID(int ID) {
        this.ID = ID;
    }
    
    public void setPriority(int priority) {
        this.priority = priority;
    }

    public void setEstimatedDuration(double estimatedDuration) {
        this.estimatedDuration = estimatedDuration;
    }

    public void setSize(double size) {
        this.size = size;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "AssignmentPOJO [ID=" + ID + ", name=" + name + ", priority="
				+ priority + ", estimatedDuration=" + estimatedDuration
				+ ", size=" + size + ", startTime=" + startTime + ", endTime="
				+ endTime + "]";
	}
    
    
    
}