/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package de.htwaalen.project.propas.common;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author norabx
 */

@XmlRootElement (name = "Resource")
public class ResourcePOJO {
    protected int ID;
    protected String name;
    protected List<String> roles;
    protected String experienceLevel;
    protected double utilizationMaximum;
    protected int wipLimit;

    @XmlElement (name = "ID")
    public int getID() {
        return ID;
    }

    @XmlElement (name = "Name")
    public String getName() {
        return name;
    }

    @XmlElement (name = "Roles")
    public List<String> getRoles() {
        return roles;
    }

    @XmlElement (name = "ExpLevel")
    public String getExperienceLevel() {
        return experienceLevel;
    }

    @XmlElement (name = "UtilMax")
    public double getUtilizationMaximum() {
        return utilizationMaximum;
    }

    @XmlElement (name = "WipLimit")
    public int getWipLimit() {
        return wipLimit;
    }
    

    public void setID(int ID) {
        this.ID = ID;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public void setExperienceLevel(String experienceLevel) {
        this.experienceLevel = experienceLevel;
    }

    public void setUtilizationMaximum(double utilizationMaximum) {
        this.utilizationMaximum = utilizationMaximum;
    }

    public void setWipLimit(int wipLimit) {
        this.wipLimit = wipLimit;
    }


	@Override
	public String toString() {
		return "ResourcePOJO [ID=" + ID + ", name=" + name + ", roles=" + roles
				+ ", experienceLevel=" + experienceLevel
				+ ", utilizationMaximum=" + utilizationMaximum + ", wipLimit="
				+ wipLimit + "]";
	}
    
    
}
