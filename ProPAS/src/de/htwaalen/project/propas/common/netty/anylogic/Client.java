package de.htwaalen.project.propas.common.netty.anylogic;

import org.jboss.netty.bootstrap.ClientBootstrap;
import org.jboss.netty.channel.ChannelFactory;
import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.channel.Channels;
import org.jboss.netty.channel.group.ChannelGroup;
import org.jboss.netty.channel.group.DefaultChannelGroup;
import org.jboss.netty.channel.socket.nio.NioClientSocketChannelFactory;

import de.htwaalen.project.propas.common.AssignmentPOJO;
import de.htwaalen.project.propas.common.PlanPOJO;
import de.htwaalen.project.propas.common.anylogic.Communication;
import de.htwaalen.project.propas.common.netty.common.*;
import de.htwaalen.project.propas.common.xml.common.XmlParser;

import java.net.InetSocketAddress;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

import javax.swing.text.html.HTMLDocument.HTMLReader.SpecialAction;


/**
 * This client allows to communicate between anylogic as client and optaplanner as server. <br>
 * To simplify the asynchronous communication does this client only allow one request at the <br>
 * same time.
 * @author Joule
 *
 */
public class Client implements ClientHandlerListener {

    // configuration --------------------------------------------------------------------------------------------------

    private final String host;
    private final int port;

    // internal vars --------------------------------------------------------------------------------------------------

    private ChannelFactory clientFactory;
    private ChannelGroup channelGroup;
    private ClientHandler handler;
	private Object response;

    // constructors ---------------------------------------------------------------------------------------------------

    public Client(String host, int port) {
        this.host = host;
        this.port = port;
    }
    

    // ClientHandlerListener ------------------------------------------------------------------------------------------

    /**
     * This method receives the messages from the server
     */
    @Override
    public void messageReceived(Envelope message) {
        //System.err.println("Received message " + message);
        //System.err.println("Payload(String) : " + new String(message.getPayload()));
        
        if(message.getVersion() == Version.INITASSIGNMENT){
        	response = XmlParser.xmlToAssignmentList(new String(message.getPayload()));
        }
        if(message.getVersion() == Version.INITRESOURCE){
        	response = XmlParser.xmlToResourceList(new String(message.getPayload()));
        }
        
        if(message.getVersion() == Version.NEWPLAN){
        	response = XmlParser.xmlToPlanPOJO(new String(message.getPayload()));
        }
        
        if(message.getVersion() == Version.ISALIVE) response = true;
    }

    // public methods -------------------------------------------------------------------------------------------------

    /**
     * This method starts the client and connect it with the server
     * @return
     * 		true  - Connection established<br>
     * 		false - Connection failed
     */
    public boolean start() {

		// For production scenarios, use limited sized thread pools
		this.clientFactory = new NioClientSocketChannelFactory(
				Executors.newCachedThreadPool(),
				Executors.newCachedThreadPool());
		this.channelGroup = new DefaultChannelGroup(this + "-channelGroup");
		this.handler = new ClientHandler(this, this.channelGroup);
		ChannelPipelineFactory pipelineFactory = new ChannelPipelineFactory() {

			@Override
			public ChannelPipeline getPipeline() throws Exception {
				ChannelPipeline pipeline = Channels.pipeline();
				pipeline.addLast("byteCounter", new ByteCounter(
						"clientByteCounter"));
				pipeline.addLast("encoder", Encoder.getInstance());
				pipeline.addLast("decoder", new Decoder());
				pipeline.addLast("handler", handler);
				return pipeline;
			}
		};
		ClientBootstrap bootstrap = new ClientBootstrap(this.clientFactory);
		bootstrap.setOption("reuseAddress", true);
		bootstrap.setOption("tcpNoDelay", true);
		bootstrap.setOption("keepAlive", true);
		bootstrap.setPipelineFactory(pipelineFactory);
		boolean connected = bootstrap
				.connect(new InetSocketAddress(host, port))
				.awaitUninterruptibly().isSuccess();
		if (!connected) {
			this.stop();
		}
		return connected;
    }

    /**
     * This method stops the connection between this client and the server
     */
    public void stop() {
        if (this.channelGroup != null) {
            this.channelGroup.close();
        }
        if (this.clientFactory != null) {
            this.clientFactory.releaseExternalResources();
        }
    }

    /**
     * This method sends a message to the server with an given parameter
     * @param version {@link Version}<br>
     * 
     */
    public void sendMessage(Version version){
    	this.handler.sendMessage(new Envelope(version, Type.REQUEST, new byte[1]));
    }
    
    public void sendAssignment(AssignmentPOJO pojo){
    	this.handler.sendMessage(new Envelope(Version.FINISHASSIGNMENT, Type.REQUEST, (XmlParser.assignmentToXML(pojo).getBytes())));
    }
    
    /**
     * Previously you have to send a message with {@link Client#sendMessage(Version)} <br>
     * afterwards it is possible with this method to check the response.
     * @return 
     * 		- the requested object or <br>
     * 		- 'null' if no response is arrived
     * 		
     */
    public Object getResponse(){
    	if(response != null) {
    		Object returnValue = response;
    		response = null;
    		return returnValue;
    	}
    	else return null;
    }

    // main -----------------------------------------------------------------------------------------------------------

    /*/
    public static void main(String[] args) throws InterruptedException {
        final Client client = new Client("localhost", 9999);

        if (!client.start()) {

            System.exit(-1);
            return; // not really needed...
        }

        System.out.println("Client started...");

        client.sendMessage(Version.INITASSIGNMENT);

        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                client.stop();
            }
        });
    }
    /*/
    
}
