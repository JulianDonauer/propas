package de.htwaalen.project.propas.common.netty.anylogic;

import de.htwaalen.project.propas.common.netty.common.Envelope;

public interface ClientHandlerListener {

    void messageReceived(Envelope message);
}
