package de.htwaalen.project.propas.common.netty.optaplanner;

import java.util.List;

import de.htwaalen.project.propas.common.AssignmentPOJO;
import de.htwaalen.project.propas.common.ResourcePOJO;
import de.htwaalen.project.propas.common.PlanPOJO;


public interface ISolver {
	PlanPOJO solving(AssignmentPOJO assipojo);
	List<AssignmentPOJO> getAssignmentList();
	List<ResourcePOJO> getResourceList();
}
