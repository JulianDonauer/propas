package de.htwaalen.project.propas.common.netty.optaplanner;

import java.util.Date;

import de.htwaalen.project.propas.common.AssignmentListPOJO;
import de.htwaalen.project.propas.common.AssignmentPOJO;
import de.htwaalen.project.propas.common.ResourceListPOJO;
import de.htwaalen.project.propas.common.ResourcePOJO;
import de.htwaalen.project.propas.common.netty.common.Envelope;
import de.htwaalen.project.propas.common.netty.common.Type;
import de.htwaalen.project.propas.common.netty.common.Version;
import de.htwaalen.project.propas.common.xml.common.XmlParser;

import org.jboss.netty.channel.ChannelFuture;
import org.jboss.netty.channel.ChannelFutureListener;
import org.jboss.netty.channel.ChannelFutureProgressListener;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ChannelStateEvent;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelUpstreamHandler;
import org.jboss.netty.channel.group.ChannelGroup;

import java.util.*;

public class ServerHandler extends SimpleChannelUpstreamHandler {

	private ISolver solver;
    // internal vars --------------------------------------------------------------------------------------------------

    private final ChannelGroup channelGroup;

    // constructors ---------------------------------------------------------------------------------------------------

    public ServerHandler(ChannelGroup channelGroup, ISolver solver) {
        this.channelGroup = channelGroup;
        this.solver = solver;
    }

    // SimpleChannelUpstreamHandler -----------------------------------------------------------------------------------

    @Override
    public void channelConnected(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
        this.channelGroup.add(e.getChannel());
    }

    
    private void initAssignments(MessageEvent e){
    	
    	for(AssignmentPOJO assi : solver.getAssignmentList()){
    		System.out.println(assi);
    	}
    	
    	e.getChannel().write(new Envelope(
    			Version.INITASSIGNMENT, Type.RESPONSE, XmlParser.assignmentListToXML(
    					new AssignmentListPOJO(solver.getAssignmentList())).getBytes()));
    }
    
    private void initResources(MessageEvent e){
    	e.getChannel().write(new Envelope(
    			Version.INITRESOURCE, Type.RESPONSE, XmlParser.resourceListToXML(
    					new ResourceListPOJO(solver.getResourceList())).getBytes()));
    }
    AssignmentPOJO finishAssignment; 
    private void finishAssignment(Envelope env){
    	finishAssignment = XmlParser.xmlToAssignment(new String(env.getPayload()));
    }
    
    private void getNewPlan(MessageEvent e){
    	e.getChannel().write(new Envelope(
    			Version.NEWPLAN, Type.RESPONSE, XmlParser.planPOJOToXML(
    					solver.solving(finishAssignment)).getBytes()));
    }
    
    /**
     * Hier darfst du dann rumspielen, hab nur bissle logic reingemacht dass ich testen kann
     */
    @Override
    public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) throws Exception {
    	if (e.getMessage() instanceof Envelope) {
    	
            Envelope env = (Envelope)e.getMessage();
            if(env.getType() == Type.REQUEST){
            	switch(env.getVersion()){
            	case INITASSIGNMENT: initAssignments(e); break;
            	case INITRESOURCE: initResources(e); break;
            	case FINISHASSIGNMENT: finishAssignment(env); break;
            	case NEWPLAN: getNewPlan(e);break;
            	case ISALIVE:e.getChannel().write(new Envelope(Version.ISALIVE, Type.RESPONSE, new byte[1]));break;
            	case UNKNOWN:;
            	default:;
            	}
            } 
        } else {
            super.messageReceived(ctx, e);
        }
    }
}
