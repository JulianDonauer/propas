package de.main;

import java.util.ArrayList;
import java.util.List;

import de.htwaalen.project.propas.common.AllocationPOJO;
import de.htwaalen.project.propas.common.AssignmentPOJO;
import de.htwaalen.project.propas.common.PlanPOJO;
import de.htwaalen.project.propas.common.ResourcePOJO;
import de.htwaalen.project.propas.common.netty.optaplanner.*;

public class PlanSolver implements ISolver{
	
	private List<AssignmentPOJO> assignmentList;
	private List<ResourcePOJO> resourceList;
	
	
	
	

	public void setAssignmentList(List<AssignmentPOJO> assignmentList) {
		this.assignmentList = assignmentList;
	}

	public void setResourceList(List<ResourcePOJO> resourceList) {
		this.resourceList = resourceList;
	}

	@Override
	public PlanPOJO solving(AssignmentPOJO assipojo) {
		
		/*
		 * c class AllocationPOJO {
	
	private int AssignmentID;
	private ArrayList<Integer> resourceIDs;
		private ArrayList<AllocationPOJO> list;
		 */
		
		PlanPOJO planpojo = new PlanPOJO();
		List<AllocationPOJO> allocationList = new ArrayList<AllocationPOJO>();
		 
		if(assipojo == null){
			
		}else{
			
			long startTime = assipojo.getStartTime().getTime();
			long endTime = assipojo.getEndTime().getTime();
			
			if(assignmentList != null){
				for(AssignmentPOJO assi : assignmentList){
					if(assi.getID() == assipojo.getID()){
						assignmentList.remove(assi);
						break;
					}
				}
				
				for(AssignmentPOJO assi : assignmentList){
					if(assi.getStartTime().getTime() <= endTime){
						AllocationPOJO allocation = new AllocationPOJO();
						allocation.setAssignmentID(assi.getID());
						System.out.println(assi);
						System.out.println(assi.getAllocatedResourceList());
						
						List<Integer> list = new ArrayList<Integer>();
						for(ResourcePOJO r : assi.getAllocatedResourceList()){
							list.add(r.getID());
						}
						
						allocation.setResourceIDs(list);
						
						allocationList.add(allocation);
					}
				}
				
				planpojo.setList(allocationList);
			}else{
				System.out.println("AssignmentList empty");
			}
		}
		
		
		return planpojo;
	}

	@Override
	public List<AssignmentPOJO> getAssignmentList() {
		// TODO Auto-generated method stub
		return assignmentList;
	}

	@Override
	public List<ResourcePOJO> getResourceList() {
		// TODO Auto-generated method stub
		return resourceList;
	}

}
