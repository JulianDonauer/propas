package de.main;

import de.htwaalen.project.propas.common.AssignmentPOJO;
import de.htwaalen.project.propas.common.ResourcePOJO;
import de.htwaalen.project.propas.common.netty.optaplanner.Server;




import java.util.*;

public class TestPlanner {

	
	public static void main(String... args){
		
		TestPlanner plan = new TestPlanner();
	}
	
	
	private PlanSolver plansolver = new PlanSolver();
	private List<AssignmentPOJO> assipojos = new ArrayList<AssignmentPOJO>();
	private List<ResourcePOJO> respojos = new ArrayList<ResourcePOJO>();
	
	public TestPlanner(){
		
	        ResourcePOJO r1 = new ResourcePOJO();
	        r1.setID(1);
	        r1.setName("Hans");
	        r1.setExperienceLevel("Senior");
	        
	        List<String> roles1 = new ArrayList<String>();
	        roles1.add("Tester");
	        roles1.add("Programmer");
	        r1.setRoles(roles1);
	        r1.setUtilizationMaximum(7);
	        r1.setWipLimit(1);
	        respojos.add(r1);
	        
	        
	        
	        ResourcePOJO r2 = new ResourcePOJO();
	        r2.setID(2);
	        r2.setName("Peter");
	        r2.setExperienceLevel("Junior");
	        
	        List<String> roles2 = new ArrayList<String>();
	        roles2.add("Architect");
	        r2.setRoles(roles2);
	        r2.setUtilizationMaximum(8);
	        r2.setWipLimit(1);
	        respojos.add(r2);
	        
	        
	        AssignmentPOJO a1 = new AssignmentPOJO();
	        a1.setID(1);
	        Calendar c1s = new GregorianCalendar(2014, 1, 28, 7, 0);
	        Calendar c1e = new GregorianCalendar(2014, 1, 28, 8, 0);
	        a1.setStartTime(c1s.getTime());
	        a1.setEndTime(c1e.getTime());
	        a1.setEstimatedDuration(60 * 1);
	        a1.setPriority(0);
	        a1.setSize(60 * 0.16);
	        List<ResourcePOJO> rl1 = new ArrayList<ResourcePOJO>();
	        rl1.add(r1);
	        a1.setAllocatedResourceList(rl1);
	        assipojos.add(a1);
	        
	        AssignmentPOJO a2 = new AssignmentPOJO();
	        a2.setID(2);
	        Calendar c2s = new GregorianCalendar(2014, 1, 28, 8, 0);
	        Calendar c2e = new GregorianCalendar(2014, 1, 28, 10, 0);
	        a2.setStartTime(c2s.getTime());
	        a2.setEndTime(c2e.getTime());
	        a2.setEstimatedDuration(60 * 2);
	        a2.setPriority(0);
	        a2.setSize(2 * 60 * 0.16);
	        List<ResourcePOJO> rl2 = new ArrayList<ResourcePOJO>();
	        rl2.add(r1);
	        a2.setAllocatedResourceList(rl2);
	        assipojos.add(a2);
	        //--------------------------------------------------------------
	        AssignmentPOJO a3 = new AssignmentPOJO();
	        a3.setID(3);
	        Calendar c3s = new GregorianCalendar(2014, 1, 28, 7, 0);
	        Calendar c3e = new GregorianCalendar(2014, 1, 28, 9, 0);
	        a3.setStartTime(c3s.getTime());
	        a3.setEndTime(c3e.getTime());
	        a3.setEstimatedDuration(60 * 2);
	        a3.setPriority(0);
	        a3.setSize(60 *2 *0.16);
	        List<ResourcePOJO> rl3 = new ArrayList<ResourcePOJO>();
	        rl3.add(r2);
	        a3.setAllocatedResourceList(rl3);
	        assipojos.add(a3);
	        
	        AssignmentPOJO a4 = new AssignmentPOJO();
	        a4.setID(4);
	        Calendar c4s = new GregorianCalendar(2014, 1, 28, 9, 0);
	        Calendar c4e = new GregorianCalendar(2014, 1, 28, 10, 0);
	        a4.setStartTime(c4s.getTime());
	        a4.setEndTime(c4e.getTime());
	        a4.setEstimatedDuration(60 * 1);
	        a4.setPriority(0);
	        a4.setSize(60 * 0.16);
	        List<ResourcePOJO> rl4 = new ArrayList<ResourcePOJO>();
	        rl4.add(r2);
	        a4.setAllocatedResourceList(rl4);
	        assipojos.add(a4);
	        
	        AssignmentPOJO a5 = new AssignmentPOJO();
	        a5.setID(5);
	        Calendar c5s = new GregorianCalendar(2014, 1, 28, 10, 0);
	        Calendar c5e = new GregorianCalendar(2014, 1, 28, 12, 0);
	        a5.setStartTime(c5s.getTime());
	        a5.setEndTime(c5e.getTime());
	        a5.setEstimatedDuration(60 * 2);
	        a5.setPriority(0);
	        a5.setSize(2 * 60 * 0.16);
	        List<ResourcePOJO> rl5 = new ArrayList<ResourcePOJO>();
	        rl5.add(r2);
	        a5.setAllocatedResourceList(rl5);
	        assipojos.add(a5);
	        		
	        for(AssignmentPOJO pojo : assipojos){
	        	System.out.println(pojo);
	        }
	        
	        plansolver.setAssignmentList(assipojos);
	        plansolver.setResourceList(respojos);
	        plansolver.solving(null);
	        
	        final Server server = new Server("localhost", 9999, true);
	        server.setSolver(plansolver);

	        if (!server.start()) {

	            System.exit(-1);
	            return; // not really needed...
	        }
	        
	        System.out.println("Server started...");

	        Runtime.getRuntime().addShutdownHook(new Thread() {
	            @Override
	            public void run() {
	                server.stop();
	            }
	        });
	        
	}
}
